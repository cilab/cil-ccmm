package edu.baylor.ecs.cil.ccmm;

import edu.baylor.ecs.cil.ccmm.actions.ControllerMethod;
import edu.baylor.ecs.cil.ccmm.actions.IBaseStatement;
import edu.baylor.ecs.cil.ccmm.actions.ResourceMethod;
import edu.baylor.ecs.cil.ccmm.actions.RestServiceMethod;
import lombok.Data;

import java.util.List;

@Data
public class AstRoot {

    private IBaseStatement root;
    private ControllerMethod controllerMethod;
    private ResourceMethod resourceMethod;
    private List<RestServiceMethod> restServiceMethods;

}
