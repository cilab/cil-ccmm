package edu.baylor.ecs.cil.ccmm.actions;

import lombok.Data;

import java.util.List;

@Data
public class CycleStatement implements IBaseStatement{
    private List<AstMethod> methods;
}
