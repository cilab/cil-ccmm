package edu.baylor.ecs.cil.ccmm.actions;

import lombok.Data;
import edu.baylor.ecs.cil.ccmm.properties.DatabaseOperation;

@Data
public class ResourceMethod extends AstMethod implements IBaseStatement {

    private DatabaseOperation operation;

}
