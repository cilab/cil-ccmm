package edu.baylor.ecs.cil.ccmm.properties;

public enum DatabaseOperation {
    CREATE, UPDATE, DELETE, GETONE, GETALL;
}
