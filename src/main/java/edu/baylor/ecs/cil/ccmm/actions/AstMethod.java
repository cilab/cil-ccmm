package edu.baylor.ecs.cil.ccmm.actions;

import lombok.Data;
import edu.baylor.ecs.cil.ccmm.properties.MetaEntity;

import java.util.List;

@Data
public abstract class AstMethod implements IBaseStatement {

    private String id;
    private String packageId;
    private String name;
    private List<MetaEntity> arguments;
    private MetaEntity returnEntity;

}
