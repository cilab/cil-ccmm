package edu.baylor.ecs.cil.ccmm.actions;

import lombok.Data;
import edu.baylor.ecs.cil.ccmm.properties.HttpMethod;

@Data
public class RestServiceMethod extends AstMethod implements IBaseStatement {

    private String ip;
    private String port;
    private HttpMethod httpMethod;

}
