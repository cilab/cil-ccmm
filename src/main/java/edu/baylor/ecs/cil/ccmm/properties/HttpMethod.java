package edu.baylor.ecs.cil.ccmm.properties;

public enum HttpMethod {

    GET, POST, DELETE, PUT
}
