package edu.baylor.ecs.cil.ccmm.properties;

import lombok.Data;

@Data
public class MetaEntity {
    private String packageId;
    private String name;
}
