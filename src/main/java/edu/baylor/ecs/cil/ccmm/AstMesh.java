package edu.baylor.ecs.cil.ccmm;

import edu.baylor.ecs.cil.ccmm.properties.MetaEntityMapping;
import lombok.Data;

import java.util.List;

@Data
public class AstMesh {

    private List<AstRoot> astRoots;

    private List<MetaEntityMapping> metaEntityMappings;

}
