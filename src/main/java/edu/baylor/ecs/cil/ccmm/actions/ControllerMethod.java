package edu.baylor.ecs.cil.ccmm.actions;

import lombok.Data;
import edu.baylor.ecs.cil.ccmm.properties.HttpMethod;

import java.util.List;

@Data
public class ControllerMethod extends AstMethod implements IBaseStatement{

    private List<String> securityRoles;
    private HttpMethod httpMethod;

}
