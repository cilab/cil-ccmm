package edu.baylor.ecs.cil.ccmm.properties;

import lombok.Data;

@Data
public class MetaEntityMapping {
    public MetaEntity a;
    public MetaEntity b;
}
