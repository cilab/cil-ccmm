package edu.baylor.ecs.cil.ccmm.actions;

import lombok.Data;

import java.util.List;

@Data
public class DecisionStatement implements IBaseStatement{

    private String condition; //optional
    private List<AstMethod> methodCalls;

}
